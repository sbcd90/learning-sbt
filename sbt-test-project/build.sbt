name := "sbt-test-project"

version := "1.0"

scalaVersion := "2.11.8"

lazy val Common = config("common") describedAs "Dependencies common to both configurations"
lazy val Spark1_6 = config("1.6") extend Common describedAs "Spark 1.6"
lazy val Spark2_0 = config("2.0") extend Common describedAs "Spark 2.0"

lazy val customCompile = config("compile") extend(Common, Spark1_6, Spark2_0)

lazy val commonSettings = Seq(
  organization := "org.apache.spark",
  version := "1.0-SNAPSHOT",
  scalaVersion := "2.11.8"
)

lazy val spark1_6 = (project in file("Spark1_6")).
                    dependsOn(takeRootSourceCode % "compile->1.6").
                    settings(commonSettings: _*)

lazy val spark2_0 = (project in file("Spark2_0")).
                    dependsOn(takeRootSourceCode % "compile->2.0").
                    settings(commonSettings: _*)

lazy val takeRootSourceCode = (project in file(".")).
                              settings(commonSettings: _*).
                              settings(inConfig(Common)(Defaults.configSettings): _*).
                              settings(addArtifact(artifact in (Common, packageBin), packageBin in Common): _*).
                              settings(
                                classpathConfiguration in Common := customCompile,

                                ivyConfigurations := overrideConfigs(Spark1_6, Spark2_0, Common, customCompile)(ivyConfigurations.value),

                                defaultConfiguration := Some(Common),

                                libraryDependencies ++= Seq(
                                  "org.apache.spark" % "spark-core_2.11" % "1.6.2" % Spark1_6,
                                  "org.apache.spark" % "spark-sql_2.11" % "1.6.2" % Spark1_6,
                                  "org.apache.spark" % "spark-core_2.11" % "2.0.0" % Spark2_0,
                                  "org.apache.spark" % "spark-sql_2.11" % "2.0.0" % Spark2_0
                                )
                              )
    