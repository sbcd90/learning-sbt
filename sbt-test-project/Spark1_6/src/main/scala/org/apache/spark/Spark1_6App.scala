package org.apache.spark

import org.apache.spark.sql.SQLContext

object Spark1_6App extends App {
  val conf = new SparkConf().setAppName("Spark1_6App").setMaster("local[*]")
  val sc = new SparkContext(conf)

  val sqlContext = new SQLContext(sc)
  import sqlContext.implicits._

  val ds = sc.parallelize(Seq(1, 2, 3, 4, 5)).toDS()
  ds.show()
}