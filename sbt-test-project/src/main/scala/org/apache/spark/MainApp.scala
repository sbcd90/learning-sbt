package org.apache.spark

import org.apache.spark.sql.SparkSession

object MainApp extends App {
  println("Hello World")

  val conf = new SparkConf().setAppName("MainApp").setMaster("local[*]")

  val sqlContext = SparkSession.builder().config(conf).getOrCreate()
  import sqlContext.implicits._

  val sc = sqlContext.sparkContext

  val ds = sc.parallelize(Seq(1, 2, 3, 4, 5)).toDS()
  ds.show()
}