package org.apache.spark

import org.apache.spark.sql.SparkSession

object Spark2_0App extends App {
  val conf = new SparkConf().setAppName("Spark2_0App").setMaster("local[*]")
  val sqlContext = SparkSession.builder().config(conf).getOrCreate()

  import sqlContext.implicits._

  val sc = sqlContext.sparkContext

  val ds = sc.parallelize(Seq(1, 2, 3, 4, 5)).toDS()

  ds.show()
}