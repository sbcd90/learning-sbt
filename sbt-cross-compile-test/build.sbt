name := "sbt-cross-compile-test"

version := "1.0"

scalaVersion := "2.11.8"

lazy val commonSettings = Seq(
  organization := "org.apache.spark",
  version := "1.0-SNAPSHOT",
  scalaVersion := "2.11.8",

  scalaSource in Compile := baseDirectory.value / ".." / ".." / "src" / "main" / "scala",
  scalaSource in Test := baseDirectory.value / ".." / ".." / "src" / "test" / "scala"
)
val projectVersion = "1.0.0"
val sparkVersionDetector = (suffix: String) => suffix

def generateProject(subProjectName: String, sparkVersion: Option[String]) = {
  val suffix = sparkVersion.getOrElse("")

  Project(subProjectName, file(s"target/${suffix}"))
    .settings(commonSettings)
    .settings(
      version := s"${projectVersion}-${suffix}",
      libraryDependencies += "org.apache.spark" % "spark-core_2.11" % sparkVersionDetector(suffix)
    )
}

lazy val projectForSpark1_6 = generateProject("spark_1_6", Some("1.6.2"))
lazy val projectForSpark2_0 = generateProject("spark_2_0", Some("2.0.0"))
    